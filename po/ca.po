# Catalan translation for gnome-shell-extension-openweather
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Pau Iranzo <paugnu@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: 3.0\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: 2012-05-13 22:37+0100\n"
"Last-Translator: Christian METZLER <neroth@xeked.com>\n"
"Language-Team: Softcatalà\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.0-rc1\n"
"X-Project-Style: default\n"

#: src/extension.js:181
msgid "..."
msgstr "..."

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org no funciona sense una clau API.\n"
"Utilitza la clau per defecte de l'extensió al diàleg de preferències o "
"registra't a https://openweahermap.org/appid i enganxa la teva clau personal "
"al diàleg de preferències."

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Dark Sky no funciona sense una clau API.\n"
"Registra't a https://darksky.net/dev/register i enganxa la teva clau "
"personal al diàleg de preferències."

#: src/extension.js:519 src/extension.js:531
#, fuzzy, javascript-format
msgid "Can not connect to %s"
msgstr "No es pot obrir %s"

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr "Ubicacions"

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr "Actualitza la informació del temps"

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr "Informació del temps proporcionada per:"

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr "No es pot obrir %s"

#: src/extension.js:887
msgid "Weather Settings"
msgstr "Preferències"

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr "Ciutat no vàlida"

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr "Ubicació no vàlida! Prova de recrear-la"

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr ""

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr ""

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr ""

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr ""

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr ""

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr ""

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr ""

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr ""

#: src/extension.js:1055
msgid "Calm"
msgstr "Calma"

#: src/extension.js:1058
msgid "Light air"
msgstr "Vent dèbil"

#: src/extension.js:1061
msgid "Light breeze"
msgstr "Brisa dèbil"

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr "Brisa suau"

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr "Brisa moderada"

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr "Brisa fresca"

#: src/extension.js:1073
msgid "Strong breeze"
msgstr "Brisa forta"

#: src/extension.js:1076
msgid "Moderate gale"
msgstr "Temporal moderat"

#: src/extension.js:1079
msgid "Fresh gale"
msgstr "Temporal"

#: src/extension.js:1082
msgid "Strong gale"
msgstr "Temporal fort"

#: src/extension.js:1085
msgid "Storm"
msgstr "Tempesta"

#: src/extension.js:1088
msgid "Violent storm"
msgstr "Tempesta intensa"

#: src/extension.js:1091
msgid "Hurricane"
msgstr "Huracà"

#: src/extension.js:1095
msgid "Sunday"
msgstr "Diumenge"

#: src/extension.js:1095
msgid "Monday"
msgstr "Dilluns"

#: src/extension.js:1095
msgid "Tuesday"
msgstr "Dimarts"

#: src/extension.js:1095
msgid "Wednesday"
msgstr "Dimecres"

#: src/extension.js:1095
msgid "Thursday"
msgstr "Dijous"

#: src/extension.js:1095
msgid "Friday"
msgstr "Divendres"

#: src/extension.js:1095
msgid "Saturday"
msgstr "Dissabte"

#: src/extension.js:1101
msgid "N"
msgstr ""

#: src/extension.js:1101
msgid "NE"
msgstr ""

#: src/extension.js:1101
msgid "E"
msgstr ""

#: src/extension.js:1101
msgid "SE"
msgstr ""

#: src/extension.js:1101
msgid "S"
msgstr ""

#: src/extension.js:1101
msgid "SW"
msgstr "SO"

#: src/extension.js:1101
msgid "W"
msgstr "O"

#: src/extension.js:1101
msgid "NW"
msgstr "NO"

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr ""

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr ""

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr ""

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr ""

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr ""

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr ""

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr ""

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr ""

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr ""

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr ""

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr ""

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr ""

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr ""

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr ""

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr ""

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr ""

#: src/extension.js:1389
msgid "Loading ..."
msgstr "S'està carregant..."

#: src/extension.js:1393
msgid "Please wait"
msgstr "Espereu"

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr "Nuvolositat:"

#: src/extension.js:1458
msgid "Humidity:"
msgstr "Humitat:"

#: src/extension.js:1462
msgid "Pressure:"
msgstr "Pressió:"

#: src/extension.js:1466
msgid "Wind:"
msgstr "Vent:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr "Ahir"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, fuzzy, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "Fa %s dies"
msgstr[1] "Fa %s dies"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ""

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr "Avui"

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr "Demà"

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, fuzzy, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "En %s dies"
msgstr[1] "En %s dies"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Tempesta amb pluja dèbil"

#: src/openweathermap_org.js:185
#, fuzzy
msgid "Thunderstorm with rain"
msgstr "Tempestes amb pluges"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Tempestes amb pluges fortes"

#: src/openweathermap_org.js:189
#, fuzzy
msgid "Light thunderstorm"
msgstr "Tempestes aïllades"

#: src/openweathermap_org.js:191
#, fuzzy
msgid "Thunderstorm"
msgstr "Tempestes"

#: src/openweathermap_org.js:193
#, fuzzy
msgid "Heavy thunderstorm"
msgstr "Tempestes elèctriques fortes"

#: src/openweathermap_org.js:195
#, fuzzy
msgid "Ragged thunderstorm"
msgstr "Tempestes aïllades"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Tempestes amb plugim dèbil"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Tempestes amb plugim"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Tempestes amb plugim persistent"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Plugim dèbil"

#: src/openweathermap_org.js:205
#, fuzzy
msgid "Drizzle"
msgstr "Plugim"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Plugim intens"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Plugim dèbil"

#: src/openweathermap_org.js:211
#, fuzzy
msgid "Drizzle rain"
msgstr "Plugim"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Plugim persistent"

#: src/openweathermap_org.js:215
#, fuzzy
msgid "Shower rain and drizzle"
msgstr "Barreja de pluja i calamarsa"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Xàfecs forts i plugim"

#: src/openweathermap_org.js:219
#, fuzzy
msgid "Shower drizzle"
msgstr "Plugim gelat"

#: src/openweathermap_org.js:221
#, fuzzy
msgid "Light rain"
msgstr "Pluja suau"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Pluja moderada"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Pluja intensa"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Pluja molt intensa"

#: src/openweathermap_org.js:229
#, fuzzy
msgid "Extreme rain"
msgstr "Pluja abundant"

#: src/openweathermap_org.js:231
#, fuzzy
msgid "Freezing rain"
msgstr "Pluja glaçada"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Xàfecs de baixa intensitat"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Xàfecs"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Xàfects de gran intensitat"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Xàfecs aïllats"

#: src/openweathermap_org.js:241
#, fuzzy
msgid "Light snow"
msgstr "Neu aixecada pel vent"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Neu"

#: src/openweathermap_org.js:245
#, fuzzy
msgid "Heavy snow"
msgstr "Nevada forta"

#: src/openweathermap_org.js:247
#, fuzzy
msgid "Sleet"
msgstr "Aiguaneu"

#: src/openweathermap_org.js:249
#, fuzzy
msgid "Shower sleet"
msgstr "Ruixats"

#: src/openweathermap_org.js:251
#, fuzzy
msgid "Light rain and snow"
msgstr "Pluja lleugera i neu"

#: src/openweathermap_org.js:253
#, fuzzy
msgid "Rain and snow"
msgstr "Pluja i neu"

#: src/openweathermap_org.js:255
#, fuzzy
msgid "Light shower snow"
msgstr "Precipitacions lleugeres de neu"

#: src/openweathermap_org.js:257
#, fuzzy
msgid "Shower snow"
msgstr "Nevades"

#: src/openweathermap_org.js:259
#, fuzzy
msgid "Heavy shower snow"
msgstr "Nevada forta"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Boirina"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Boira"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Calitja"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Remolins d'arena"

#: src/openweathermap_org.js:269
#, fuzzy
msgid "Fog"
msgstr "Boirós"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Arena"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Pols"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "CENDRA VOLCÀNICA"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "TORBONADA"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "TORNADO"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Cel clar"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Alguns núvols"

#: src/openweathermap_org.js:285
#, fuzzy
msgid "Scattered clouds"
msgstr "Núvols dispersos"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Núvols trencats"

#: src/openweathermap_org.js:289
#, fuzzy
msgid "Overcast clouds"
msgstr "Majoritàriament ennuvolat"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "No disponible"

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
#, fuzzy
msgid "Searching ..."
msgstr "S'està carregant..."

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Dades no vàlides en buscar \"%s\""

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, fuzzy, javascript-format
msgid "\"%s\" not found"
msgstr "No s'ha trobat \"%s\". "

#: src/prefs.js:232
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Clau Api personal de openweathermap.org"

#: src/prefs.js:233
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Clau Api personal de openweathermap.org"

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
#, fuzzy
msgid "Location"
msgstr "Ubicacions"

#: src/prefs.js:350
msgid "Provider"
msgstr "Proveïdor"

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr "Voleu esborrar %s ?"

#: src/prefs.js:563
msgid "No"
msgstr ""

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr "per defecte"

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr "Edita el nom"

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
#, fuzzy
msgid "Clear entry"
msgstr "Neteja l'entrada"

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr "Edita les coordenades"

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr "Proveïdor del temps per defecte de l'extensió"

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr "Cancel·la"

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr "Desa"

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr "Cerca per ubicació o coordenades"

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "ex. Vaiaku, Tuvalu o -8.5211767,179.1976747"

#: data/weather-settings.ui:184
msgid "Find"
msgstr "Busca"

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr "Tria el proveïdor per defecte"

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr "Clau Api personal de openweathermap.org"

#: data/weather-settings.ui:372
#, fuzzy
msgid "Personal Api key from Dark Sky"
msgstr "Clau Api personal de openweathermap.org"

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr "Temps restant per actualitzar el temps actual [min]"

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr "Temps restant per actualitzar la previsió del temps [min]"

#: data/weather-settings.ui:418
#, fuzzy
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Nota: el forecast-timout no s'utilitza per a Dark Sky, perque no proveeixen "
"descàrregues separades pel temps actual i les previsions."

#: data/weather-settings.ui:441
#, fuzzy
msgid "Use extensions api-key for openweathermap.org"
msgstr "Clau Api personal de openweathermap.org"

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Desmarca, si tens la teva clau API per a openweathermap.org i posa-la dins "
"el quadre de text de sota."

#: data/weather-settings.ui:462
#, fuzzy
msgid "Weather provider"
msgstr "Informació del temps proporcionada per:"

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr "Tria proveïdor de geolocalització"

#: data/weather-settings.ui:500
#, fuzzy
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Clau Api personal de openweathermap.org"

#: data/weather-settings.ui:522
#, fuzzy
msgid "Geolocation provider"
msgstr "Informació del temps proporcionada per:"

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Temperatura:"

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr "Velocitat del vent:"

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
#, fuzzy
msgid "Pressure Unit"
msgstr "Pressió:"

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr ""

#: data/weather-settings.ui:622
msgid "Units"
msgstr "Unitats"

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Posició:"

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr ""

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Direcció del vent amb fletxes"

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Condicions climàtiques traduïdes"

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Icones simbòliques"

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr "Text sobre els botons"

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Temperatura:"

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Condicions climàtiques:"

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
#, fuzzy
msgid "Conditions in Forecast"
msgstr "Previsió meteorològica:"

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr "Centrar les previsions"

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Nombre de dies de les previsions"

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Nombre màxim de posicions decimals"

#: data/weather-settings.ui:747
msgid "Center"
msgstr "Centre"

#: data/weather-settings.ui:748
msgid "Right"
msgstr "Dreta"

#: data/weather-settings.ui:749
msgid "Left"
msgstr "Esquerra"

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr "Plantilla"

#: data/weather-settings.ui:935
msgid "Version: "
msgstr "Versió: "

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr "desconeguda (auto-build ?)"

#: data/weather-settings.ui:949
#, fuzzy
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Extensió climatològica per mostrar informació del temps des de <a href="
"\"https://openweathermap.org/\">Openweathermap</a> o <a href=\"https://"
"darksky.net\">Dark Sky</a> per gairebé qualsevol lloc del món.</span>"

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr "Mantinguda per"

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr "Pàgina web"

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Aquest programa NO TÉ CAP GARANTIA.\n"
"Mireu la <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> per a més detall.</"
"span>"

#: data/weather-settings.ui:997
msgid "About"
msgstr "Quant a"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
#, fuzzy
msgid "Weather Provider"
msgstr "Informació del temps proporcionada per:"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
#, fuzzy
msgid "Geolocation Provider"
msgstr "Informació del temps proporcionada per:"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
#, fuzzy
msgid "Wind Speed Units"
msgstr "Velocitat del vent:"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
#, fuzzy
msgid "Use text on buttons in menu"
msgstr "Text sobre els botons"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
#, fuzzy
msgid "Refresh interval (actual weather)"
msgstr "Centrar les previsions"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
#, fuzzy
msgid "Refresh interval (forecast)"
msgstr "Centrar les previsions"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
#, fuzzy
msgid "Center forecastbox."
msgstr "Centrar les previsions"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
#, fuzzy
msgid "Your personal API key from openweathermap.org"
msgstr "Clau Api personal de openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
#, fuzzy
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Clau Api personal de openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
#, fuzzy
msgid "Your personal API key from Dark Sky"
msgstr "Clau Api personal de openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
#, fuzzy
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Clau Api personal de openweathermap.org"
