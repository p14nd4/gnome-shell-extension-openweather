# Czech translation for gnome-shell-extension-openweather
# Copyright (C) 2011-2014
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# David Štancl <dstancl@dstancl.cz>, 2011.
# Lukáš Maňas <lukas.manas@gmail.com>, 2012.
# František Zatloukal <zatloukal.frantisek@gmail.com>, 2014.
# Petr Pulc <petrpulc@gmail.com>, 2016, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: 2017-09-18 12:43+0200\n"
"Last-Translator: Petr Pulc <petrpulc@gmail.com>\n"
"Language-Team: cs_CZ <czech@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 1.8.11\n"

#: src/extension.js:181
msgid "..."
msgstr "…"

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Zdroj Openweathermap.org nefunguje bez API klíče.\n"
"Buď zapněte v nastavení přepínač pro výchozí klíč rozšíření, nebo se "
"registrujte na https://openweathermap.org/appid a vložte váš osobní klíč do "
"příslušného pole v dialogu nastavení."

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Zdroj Dark Sky nefunguje bez API klíče.\n"
"Prosím, registrujte se na https://darksky.net/dev/register a vložte váš "
"osobní klíč do příslušného pole v dialogu nastavení."

#: src/extension.js:519 src/extension.js:531
#, javascript-format
msgid "Can not connect to %s"
msgstr "Nemohu se připojit k %s"

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr "Místa"

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr "Obnovit informace o počasí"

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr "Data o počasí poskytuje:"

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr "Nemohu otevřít %s"

#: src/extension.js:887
msgid "Weather Settings"
msgstr "Nastavení"

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr "Neplatné město"

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr "Neplatné umístění! Prosím, zkuste ho vytvořit znovu."

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr "°F"

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr "K"

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr "°De"

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr "°N"

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr "°C"

#: src/extension.js:1055
msgid "Calm"
msgstr "Bezvětří"

#: src/extension.js:1058
msgid "Light air"
msgstr "Vánek"

#: src/extension.js:1061
msgid "Light breeze"
msgstr "Větřík"

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr "Slabý vítr"

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr "Mírný vítr"

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr "Čerstvý vítr"

#: src/extension.js:1073
msgid "Strong breeze"
msgstr "Silný vítr"

#: src/extension.js:1076
msgid "Moderate gale"
msgstr "Mírný vichr"

#: src/extension.js:1079
msgid "Fresh gale"
msgstr "Čerstvý vichr"

#: src/extension.js:1082
msgid "Strong gale"
msgstr "Silný vichr"

#: src/extension.js:1085
msgid "Storm"
msgstr "Plný vichr"

#: src/extension.js:1088
msgid "Violent storm"
msgstr "Vichřice"

#: src/extension.js:1091
msgid "Hurricane"
msgstr "Orkán"

#: src/extension.js:1095
msgid "Sunday"
msgstr "Neděle"

#: src/extension.js:1095
msgid "Monday"
msgstr "Pondělí"

#: src/extension.js:1095
msgid "Tuesday"
msgstr "Úterý"

#: src/extension.js:1095
msgid "Wednesday"
msgstr "Středa"

#: src/extension.js:1095
msgid "Thursday"
msgstr "Čtvrtek"

#: src/extension.js:1095
msgid "Friday"
msgstr "Pátek"

#: src/extension.js:1095
msgid "Saturday"
msgstr "Sobota"

#: src/extension.js:1101
msgid "N"
msgstr "S"

#: src/extension.js:1101
msgid "NE"
msgstr "SV"

#: src/extension.js:1101
msgid "E"
msgstr "V"

#: src/extension.js:1101
msgid "SE"
msgstr "JV"

#: src/extension.js:1101
msgid "S"
msgstr "J"

#: src/extension.js:1101
msgid "SW"
msgstr "JZ"

#: src/extension.js:1101
msgid "W"
msgstr "Z"

#: src/extension.js:1101
msgid "NW"
msgstr "SZ"

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr "bar"

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr "atm"

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr "at"

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr "psi"

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr "mph"

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr "kn"

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1389
msgid "Loading ..."
msgstr "Načítání…"

#: src/extension.js:1393
msgid "Please wait"
msgstr "Prosím, čekejte"

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr "Oblačnost:"

#: src/extension.js:1458
msgid "Humidity:"
msgstr "Vlhkost:"

#: src/extension.js:1462
msgid "Pressure:"
msgstr "Tlak:"

#: src/extension.js:1466
msgid "Wind:"
msgstr "Vítr:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr "Včera"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "Před %d dnem"
msgstr[1] "Před %d dny"
msgstr[2] "Před %d dny"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ", "

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr "Dnes"

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr "Zítra"

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "Za %d den"
msgstr[1] "Za %d dny"
msgstr[2] "Za %d dní"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Bouřky a mírný déšť"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "Bouřky s deštěm"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Bouřky a silný déšť"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "Mírné bouřky"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "Bouřky"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "Četné bouřky"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "Ojedinělé bouřky"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Bouřky a mírné mrholení"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Bouřky a mrholení"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Bouřky a silné mrholení"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Slabé mrholení"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "Mrholení"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Silné mrholení"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Slabé mrholení s deštěm"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "Mrholení s deštěm"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Silné mrholení s deštěm"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "Přeháňky a mrholení"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Silné přeháňky a mrholení"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "Občasné mrholení"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "Mírný déšť"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Střední déšť"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Silný déšť"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Velmi silný déšť"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "Intenzivní déšť"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "Namrzající déšť"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Mírné přeháňky"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Přeháňky"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Silné přeháňky"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Ojedinělé přeháňky"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "Mírné sněžení"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Sněžení"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "Husté sněžení"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "Plískanice"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "Přeháňky se sněhem"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "Mírný déšť se sněhem"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "Déšť se sněhem"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "Mírné sněhové přeháňky"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "Sněhové přeháňky"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "Husté sněhové přeháňky"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Mlha"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Smog"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Opar"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Písečné/prachové víry"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "Mlha"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Písek"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Prach"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "SOPEČNÝ POPEL"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "NÁPORY VĚTRU"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "TORNÁDO"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Jasno"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Skoro jasno"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "Polojasno"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Oblačno"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "Zataženo"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Není k dispozici"

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
#, fuzzy
msgid "Searching ..."
msgstr "Načítání…"

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Neplatná data při hledání \"%s\""

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, javascript-format
msgid "\"%s\" not found"
msgstr "Místo \"%s\" nenalezeno."

#: src/prefs.js:232
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Váš osobní App klíč pro developer.mapquest.com"

#: src/prefs.js:233
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Osobní App klíč pro developer.mapquest.com"

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
msgid "Location"
msgstr "Místo"

#: src/prefs.js:350
msgid "Provider"
msgstr "Poskytovatel"

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr "Odstranit %s?"

#: src/prefs.js:563
#, fuzzy
msgid "No"
msgstr "S"

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr "výchozí"

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr "Upravit název"

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
msgid "Clear entry"
msgstr "Smazat položku"

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr "Upravit souřadnice"

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr "Výchozí poskytovatel předpovědi"

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr "Zrušit"

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr "Uložit"

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr "Hledat podle umístění nebo souřadnic"

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "např. Vaiaku, Tuvalu nebo -8.5211767,179.1976747"

#: data/weather-settings.ui:184
msgid "Find"
msgstr "Hledat"

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr "Vyberte výchozího poskytovatele předpovědi"

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr "Osobní API klíč pro openweathermap.org"

#: data/weather-settings.ui:372
msgid "Personal Api key from Dark Sky"
msgstr "Osobní API klíč pro Dark Sky"

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr "Obnovit aktuální stav počasí po [min]"

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr "Obnovit předpověď počasí po [min]"

#: data/weather-settings.ui:418
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Poznámka: čas pro obnovu předpovědi není v případě služby Dark Sky použit, "
"jelikož služba nenabízí informace o počasí a předpovědi odděleně."

#: data/weather-settings.ui:441
msgid "Use extensions api-key for openweathermap.org"
msgstr "Použít API klíč rozšíření pro openweathermap.org"

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Vypněte, pokud máte svůj vlastní API klíč pro openweathermap.org, a vyplňte "
"jej do pole níže."

#: data/weather-settings.ui:462
msgid "Weather provider"
msgstr "Poskytovatel počasí"

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr "Vyberte poskytovatele geolokace"

#: data/weather-settings.ui:500
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Osobní App klíč pro developer.mapquest.com"

#: data/weather-settings.ui:522
msgid "Geolocation provider"
msgstr "Poskytovatel geolokace"

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Jednotka teploty"

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr "Jednotka rychlosti větru"

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Jednotka tlaku"

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr "Beaufort"

#: data/weather-settings.ui:622
msgid "Units"
msgstr "Jednotky"

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Umístění na panelu"

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Zarovnání nabídky [v %] od 0 (vlevo) do 100 (vpravo)"

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Směr větru jako šipku"

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Překládat stav počasí"

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Symbolické ikony"

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr "Text v tlačítcích"

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Teplota v panelu"

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Stav počasí v panelu"

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Stav počasí v předpovědi"

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr "Vystředit předpověď"

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Počet dní předpovědi"

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Maximální počet číslic za desetinnou čárkou"

#: data/weather-settings.ui:747
msgid "Center"
msgstr "Vprostřed"

#: data/weather-settings.ui:748
msgid "Right"
msgstr "Vpravo"

#: data/weather-settings.ui:749
msgid "Left"
msgstr "Vlevo"

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr "Rozložení"

#: data/weather-settings.ui:935
msgid "Version: "
msgstr "Verze: "

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr "neznámá (vlastní ?)"

#: data/weather-settings.ui:949
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Rozšíření pro zobrazení informací o počasí z <a href=\"https://"
"openweathermap.org/\">Openweathermap</a> nebo <a href=\"https://darksky.net"
"\">Dark Sky</a> pro téměř všechny oblasti světa.</span>"

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr "Spravuje"

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr "Webová stránka"

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Tento program je ABSOLUTNĚ BEZ ZÁRUKY.\n"
"Více informací získáte v <a href=\"https://www.gnu.org/licenses/old-licenses/"
"gpl-2.0.html\">GNU General Public License, verze 2 nebo novější</a>.</span>"

#: data/weather-settings.ui:997
msgid "About"
msgstr "O aplikaci"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Poskytovatel počasí"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Poskytovatel geolokace"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Jednotky rychlosti větru"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Vyberte jednotky použité pro zobrazení rychlosti větru. Povolené hodnoty "
"jsou 'kph', 'mph', 'm/s', 'knots', 'ft/s' nebo 'Beaufort'."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Vyberte zda má být směr větru zobrazen pomocí šipky nebo písmene."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Zobrazované město"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Aktuální město"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Zobrazit text na tlačítkách v nabídce"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Vodorovná pozice nabídky."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Interval obnovy (aktuální počasí)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Interval obnovy (předpověď)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Vystředit předpověď."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Váš osobní API klíč pro openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Použít výchozí API klíč tohoto rozšíření pro openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Váš osobní API klíč pro Dark Sky"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Váš osobní App klíč pro developer.mapquest.com"
